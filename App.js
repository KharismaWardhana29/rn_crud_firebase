/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
import LoginForm from './src/components/LoginForm';
import Router from './src/Router';

export default class App extends Component<{}> {
  componentWillMount() {
    const config = {
		apiKey: "AIzaSyCcc5N3vbKJqSrvB_CoAYdi_agIPsP196g",
		authDomain: "reactnative-crud.firebaseapp.com",
		databaseURL: "https://reactnative-crud.firebaseio.com",
		projectId: "reactnative-crud",
		storageBucket: "reactnative-crud.appspot.com",
		messagingSenderId: "869591356399"
    };
    firebase.initializeApp(config);
  }

  render() { 
    return (
      <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
        <Router />
      </Provider>
    );
  }
}
